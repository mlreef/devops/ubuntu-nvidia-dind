#!/bin/sh
# change to the repository root folder via the scripts location
cd "$(dirname "$0")"/..
. bin/includes/ci-environment
. bin/includes/docker-registry-login
########################################
set -x    # output all commands
set -e    # exit on immediately on every error
set -u    # error on usage of undefined variables
########################################

# ensure that there is a Dockerfile, else exit
if [ ! -e ubuntu-nvidia-dind/Dockerfile ]; then exit 1; fi

BUILD_DATE=$(date -u +"%Y-%m-%dT%H:%M:%SZ")

CUDA_BASE_IMAGE="nvidia/cuda:${version}-base"
CUDA_RUNTIME_IMAGE="nvidia/cuda:${version}-runtime"
CUDA_CUDNN_RUNTIME_IMAGE="nvidia/cuda:${version}-cudnn${cudnn_version}-runtime"
DOCKER_CE_VERSION="${docker_ce_version}"

# Building and pushing UBUNTU_NVIDIA_DIND_BASE image
docker build --tag "$GITLAB_RUNNER_UBUNTU_NVIDIA_DIND_BASE_IMAGE_PATH" -f ubuntu-nvidia-dind/Dockerfile ubuntu-nvidia-dind/.  \
  --build-arg CUDA_BASE_IMAGE="$CUDA_BASE_IMAGE"          \
  --build-arg DOCKER_CE_VERSION="$DOCKER_CE_VERSION"      \
  --build-arg CI_COMMIT_REF_SLUG="$CI_COMMIT_REF_SLUG"    \
  --build-arg BUILD_DATE="$BUILD_DATE"

docker push "$GITLAB_RUNNER_UBUNTU_NVIDIA_DIND_BASE_IMAGE_PATH"

# Building and pushing UBUNTU_NVIDIA_DIND_RUNTIME image
docker build --tag "$GITLAB_RUNNER_UBUNTU_NVIDIA_DIND_RUNTIME_IMAGE_PATH" -f ubuntu-nvidia-dind/Dockerfile ubuntu-nvidia-dind/.  \
  --build-arg CUDA_BASE_IMAGE="$CUDA_RUNTIME_IMAGE"          \
  --build-arg DOCKER_CE_VERSION="$DOCKER_CE_VERSION"      \
  --build-arg CI_COMMIT_REF_SLUG="$CI_COMMIT_REF_SLUG"    \
  --build-arg BUILD_DATE="$BUILD_DATE"

docker push "$GITLAB_RUNNER_UBUNTU_NVIDIA_DIND_RUNTIME_IMAGE_PATH"

# Building and pushing UBUNTU_NVIDIA_DIND_CUDNN_RUNTIME image
docker build --tag "$GITLAB_RUNNER_UBUNTU_NVIDIA_DIND_CUDNN_RUNTIME_IMAGE_PATH" -f ubuntu-nvidia-dind/Dockerfile ubuntu-nvidia-dind/.  \
  --build-arg CUDA_BASE_IMAGE="$CUDA_CUDNN_RUNTIME_IMAGE" \
  --build-arg DOCKER_CE_VERSION="$DOCKER_CE_VERSION"      \
  --build-arg CI_COMMIT_REF_SLUG="$CI_COMMIT_REF_SLUG"    \
  --build-arg BUILD_DATE="$BUILD_DATE"

docker push "$GITLAB_RUNNER_UBUNTU_NVIDIA_DIND_CUDNN_RUNTIME_IMAGE_PATH"


