# ubuntu-nvidia-dind images

The project includes different versions of ubuntu-nvidia-dind image creation. It picked up the version from `CUDA-VERSION` file,
and a separate branch is maintained for each version. The latest version is merged to master branch and the image with latest tag 
pointing to that version gets created. 

The Nvidia Dind for base image for gitlab runner with GPU support for executing experiments. It has cuda-base as base image.

* ubuntu-nvidia-dind-base

The Nvidia Dind for base image for gitlab runner with GPU support for executing experiments. It has cuda-runner as base image.

* ubuntu-nvidia-dind-runner


